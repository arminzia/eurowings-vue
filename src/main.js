import 'es6-promise/auto'
import 'bootstrap/dist/css/bootstrap.min.css'

import Vue from 'vue'
import VueRouter from 'vue-router'

import App from './App'
import AppHome from './components/home'
import AppDetails from './components/details'
import AppAbout from './components/about'

import store from './store/store'

Vue.config.productionTip = false
Vue.use(VueRouter)

const routes = [
  { path: '/', component: AppHome },
  { path: '/home', name: 'home', component: AppHome },
  { path: '/details/:id', name: 'details', component: AppDetails },
  { path: '/about', name: 'about', component: AppAbout }
]

const router = new VueRouter({
  routes,
  linkActiveClass: "active",
  linkExactActiveClass: "exact-active"
})

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),
}).$mount('#app')
