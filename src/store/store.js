// https://www.codementor.io/@petarvukasinovic/redux-vs-vuex-for-state-management-in-vue-js-n10yd7g2f

import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    isLoading: false,

    section: 'hot',
    sort: 'viral',
    showViral: true,
    window: 'day',

    galleryItems: []
  },

  getters: {

  },

  mutations: {
    changeIsLoading(state, value) {
      state.isLoading = value
    },

    changeSection(state, value) {
      state.section = value
    },

    changeSort(state, value) {
      state.sort = value
    },

    changeWindow(state, value) {
      state.window = value
    },

    changeShowViral(state, value) {
      state.showViral = value
    },

    addGalleryItems(state, items) {
      state.galleryItems = items
    }
  },

  actions: {
    async loadGallery(context) {
      // api url: https://api.imgur.com/3/gallery/{section}/{sort}/{window}/{page}?showViral=bool

      const clientId = '6a81876548b8f9a';
      const url = `https://api.imgur.com/3/gallery/${this.state.section}/${this.state.sort}/${this.state.window}?showViral=${this.state.showViral}`;

      try {
        context.commit('changeIsLoading', true)

        const response = await fetch(url, {
          method: 'GET',
          mode: 'cors',
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Client-ID ' + clientId
          }
        });

        if (!response.ok) {
          throw new Error('Network response was not ok.');
        }

        const result = await response.json();
        const items = result.data;
        
        items.forEach(element => {
          if (element.is_album === true) {
            const cover = element.images[0];
            element.mediaUrl = cover.link;
            element.mediaType = cover.type;
          } else {
            element.mediaUrl = element.link;
            element.mediaType = element.type;
          }
        });

        context.commit('addGalleryItems', items)
      }
      catch (error) {
        console.log('There was a problem with the fetch operation: ', error.message);
      }
      finally {
        context.commit('changeIsLoading', false)
      }
    }
  }
})